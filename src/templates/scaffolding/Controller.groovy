<%=packageName ? "package ${packageName}\n\n" : ''%>


import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ${className}Controller {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
//    def springSecurityService

    @Secured(['ROLE_ADMIN'])
    def index(Integer max) {
        params?.max = Math?.min(max ?: 10, 100)
        respond ${className}?.list(params), model:[${propertyName}Count: ${className}?.count()]
    }

    @Secured(['ROLE_ADMIN'])
    def show(${className} ${propertyName}) {
        respond ${propertyName}
    }

    @Secured(['ROLE_ADMIN'])
    def create() {
        respond new ${className}(params)
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def save(${className} ${propertyName}) {
        if (${propertyName} == null) {
            notFound()
            return
        }

        if (${propertyName}?.hasErrors()) {
            respond ${propertyName}?.errors, view:'create'
            return
        }

        ${propertyName}?.save flush:true

        request?.withFormat {
            form multipartForm {
                flash?.message = message(code: 'default.created.message', args: [message(code: '${domainClass?.propertyName}.label', default: '${className}'), ${propertyName}?.id])
                redirect ${propertyName}
            }
            '*' { respond ${propertyName}, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit(${className} ${propertyName}) {
        respond ${propertyName}
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def update(${className} ${propertyName}) {
        if (${propertyName} == null) {
            notFound()
            return
        }

        if (${propertyName}?.hasErrors()) {
            respond ${propertyName}?.errors, view:'edit'
            return
        }

        ${propertyName}?.save flush:true

        request?.withFormat {
            form multipartForm {
                flash?.message = message(code: 'default.updated.message', args: [message(code: '${className}.label', default: '${className}'), ${propertyName}?.id])
                redirect ${propertyName}
            }
            '*'{ respond ${propertyName}, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN'])
    @Transactional
    def delete(${className} ${propertyName}) {

        if (${propertyName} == null) {
            notFound()
            return
        }

        ${propertyName}?.delete flush:true

        request?.withFormat {
            form multipartForm {
                flash?.message = message(code: 'default.deleted.message', args: [message(code: '${className}.label', default: '${className}'), ${propertyName}?.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request?.withFormat {
            form multipartForm {
                flash?.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass?.propertyName}.label', default: '${className}'), params?.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
