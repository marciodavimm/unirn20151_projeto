// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery
// require plugins/jQuery/jQuery-2.1.3
//= require jquery-ui
//
// require plugins/jQueryUI/jquery-ui.1.10.3
//
//= require bootstrap
//
// require plugins/morris/morris
// require plugins/sparkline/jquery.sparkline
//
// require plugins/jvectormap/jquery-jvectormap-1.2.2
// require plugins/jvectormap/jquery-jvectormap-world-mill.en
// require plugins/knob/jquery.knob
//
//= require plugins/daterangepicker/daterangepicker
//= require plugins/datepicker/bootstrap-datepicker
//
// require plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all
//
//= require plugins/iCheck/icheck
//= require plugins/slimScroll/jquery.slimscroll
//= require plugins/fastclick/fastclick
//
//= require plugins/bootstrap-slider/bootstrap-slider
// require plugins/chartjs/Chart
// require plugins/colorpicker/bootstrap-colorpicker
//= require plugins/datatables/jquery.dataTables
//= require plugins/datatables/dataTables.bootstrap
//
//= require_tree plugins/datepicker/locales
//
//= require app
// require pages/dashboard
// require demo
//
// require plugins/flot/excanvas
// require plugins/flot/jquery.colorhelpers
// require plugins/flot/jquery.flot
// require plugins/flot/jquery.flot.canvas
// require plugins/flot/jquery.flot.categories
// require plugins/flot/jquery.flot.crosshair
// require plugins/flot/jquery.flot.errorbars
// require plugins/flot/jquery.flot.fillbetween
// require plugins/flot/jquery.flot.image
// require plugins/flot/jquery.flot.navigate
// require plugins/flot/jquery.flot.pie
// require plugins/flot/jquery.flot.resize
// require plugins/flot/jquery.flot.selection
// require plugins/flot/jquery.flot.stack
// require plugins/flot/jquery.flot.symbol
// require plugins/flot/jquery.flot.threshold
// require plugins/flot/jquery.flot.time
//
// require plugins/fullcalendar/fulllcalendar
//
// require plugins/input-mask/jquery.inputmask
// require plugins/input-mask/jquery.inputmask.extensions
// require plugins/input-mask/jquery.inputmask.date.extensions
// require plugins/input-mask/jquery.inputmask.numeric.extensions
// require plugins/input-mask/jquery.inputmask.phone.extensions
// require plugins/input-mask/jquery.inputmask.regex.extensions
//
// require plugins/ionslider/ion.rangeSlider
//
// require plugins/pace/pace
// require plugins/timepicker/bootstrap-timepicker
//
// require_tree .
//= require_self

$(document).ready(function(){
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    //$.widget.bridge('uibutton', $.ui.button);

    //Activate the iCheck Plugin
    $('input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });


    $('.daterange').daterangepicker(
        {
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            startDate: moment().subtract('days', 29),
            endDate: moment()
        },
        function (start, end) {
            alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );


    //SLIMSCROLL FOR CHAT WIDGET
    $('#mainChatBoxId').slimScroll({
        height: '350px'
    });

    $("form[name=addPostagemForm]").submit(postagem.submeterFormPostagem);
});

var postagem = {
    submeterFormPostagem : function(e){
        e.preventDefault();
        var uri = $(this).data('uri');
        var dados = $(this).serializeArray();
        var destino = $(this).data('destino');

        $.ajax(uri,
            {
                data: dados,
                method:"POST"
            })
            .success(function(retorno){
                $("#" + destino).html(retorno);
                alert("Resultado:OK\n " );
                $("form[name=addPostagemForm]")[0].reset();
            })
            .fail(function(retorno){
                alert("ERRO...");
            });

    }

};
