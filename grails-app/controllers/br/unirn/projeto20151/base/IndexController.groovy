package br.unirn.projeto20151.base

import br.unirn.projeto20151.conteudo.Postagem
import br.unirn.projeto20151.security.User
import grails.plugin.springsecurity.annotation.Secured

class IndexController {

//    static scaffold = true
//    def springSecurityService

    def springSecurityService

    @Secured(['ROLE_ADMIN', 'ROLE_PARENT', 'ROLE_CHILD'])
    def index() {
        def user = springSecurityService.currentUser
        def familias = []
        def parentes = []
        Familia.list().each { familia ->
            if(familia?.integrantes?.contains(user) ) {
                familias += [familia]
                familia?.integrantes?.each { membro ->
                    parentes += [membro]
                }
            }
        }
        def ultimasPostagens = Postagem.findAllByDonoInList(parentes, [max: 20, offset: 0, sort: "dateCreated", order: "desc"])

        [usuarioEstaLogado:springSecurityService.loggedIn, ultimasPostagens:ultimasPostagens]
    }
}
