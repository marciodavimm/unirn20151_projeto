package br.unirn.projeto20151.conteudo

import br.unirn.projeto20151.base.Familia
import br.unirn.projeto20151.security.User
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class PostagemController {

    static scaffold = true
//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    @Secured(['ROLE_ADMIN', 'ROLE_PARENT', 'ROLE_CHILD'])
    def meusPosts(){
        def resultado = Postagem.findAllByDono(springSecurityService?.currentUser)
        render view: 'index' , model: [postagemInstanceList : resultado]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_PARENT', 'ROLE_CHILD'])
    def buscar(){
        redirect(action: 'meusPosts')
    }

    @Secured(['ROLE_ADMIN', 'ROLE_PARENT', 'ROLE_CHILD'])
    @Transactional
    def cadastrarPostagem(){

        def postagem = new Postagem()
        postagem.titulo = params?.titulo
        postagem.descricao = params?.descricao
        postagem.dono = springSecurityService?.currentUser
        def tags = params?.tags?.toString()?.split(",")?.toList()
        tags.each {
            def tag = Tag.findByNome(it) ?: new Tag(nome: it).save(flush: true)
            postagem.addToTags(tag)
        }
        postagem.save flush: true
        if(postagem.hasErrors()){
            println "\n\n erros: \n"
            postagem.errors.each {println it}
        }

        def familias = []
        def parentes = []
        Familia.list().each { familia ->
            if(familia?.integrantes?.contains(postagem.dono) ) {
                familias += [familia]
                familia?.integrantes?.each { membro ->
                    parentes += [membro]
                }
            }
        }
        println "\n\n parentes: \n"
        parentes.each {println it}

        def ultimasPostagens = Postagem.findAllByDonoInList(parentes, [max: 20, offset: 0, sort: "dateCreated", order: "desc"])

        render template: '/index/chatBox', model: [ultimasPostagens:ultimasPostagens]
    }

/*

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Postagem.list(params), model:[postagemInstanceCount: Postagem.count()]
    }

    def show(Postagem postagemInstance) {
        respond postagemInstance
    }

    def create() {
        respond new Postagem(params)
    }

    @Transactional
    def save(Postagem postagemInstance) {
        if (postagemInstance == null) {
            notFound()
            return
        }

        if (postagemInstance.hasErrors()) {
            respond postagemInstance.errors, view:'create'
            return
        }

        postagemInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'postagem.label', default: 'Postagem'), postagemInstance.id])
                redirect postagemInstance
            }
            '*' { respond postagemInstance, [status: CREATED] }
        }
    }

    def edit(Postagem postagemInstance) {
        respond postagemInstance
    }

    @Transactional
    def update(Postagem postagemInstance) {
        if (postagemInstance == null) {
            notFound()
            return
        }

        if (postagemInstance.hasErrors()) {
            respond postagemInstance.errors, view:'edit'
            return
        }

        postagemInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Postagem.label', default: 'Postagem'), postagemInstance.id])
                redirect postagemInstance
            }
            '*'{ respond postagemInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Postagem postagemInstance) {

        if (postagemInstance == null) {
            notFound()
            return
        }

        postagemInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Postagem.label', default: 'Postagem'), postagemInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'postagem.label', default: 'Postagem'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
*/
}
