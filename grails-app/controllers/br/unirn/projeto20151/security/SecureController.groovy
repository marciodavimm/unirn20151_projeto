package br.unirn.projeto20151.security

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
class SecureController {

//    static scaffold = true

    def springSecurityService

    @Secured(['ROLE_ADMIN'])
    def index() {
        render "Apenas pessoal autorizado"
    }

    @Secured(['ROLE_PARENT', 'ROLE_CHILD', 'ROLE_ADMIN'])
    def logout(){
        session?.invalidate()
        redirect uri: '/'
    }

}
