<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="AdminLTE 2 | Dashboard"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
    <asset:stylesheet src="application.css"/>
    <asset:javascript src="application.js"/>

    <g:layoutHead/>

<!-- Ionicons 2.0.0 -->
%{--<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />--}%
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
%{--<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>--}%
%{--<![endif]-->--}%

</head>

<body id="layoutMainBodyId" class="skin-blue">
<sec:ifNotLoggedIn>
    <script>
        console.log("usuario nao logado...");
        $("#layoutMainBodyId").addClass("sidebar-collapse");
    </script>
</sec:ifNotLoggedIn>

<div class="wrapper">

    <g:render template="/layouts/topBar"/>

    <sec:ifLoggedIn>
        <g:render template="/layouts/leftBar"/>
    </sec:ifLoggedIn>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div id="pageBodyId">
            <g:layoutBody/>
        </div>
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Versão</b> 1.0
        </div>
        <strong>Copyright &copy; 2015 <a href="javascript:;">bsi-unirn</a>.</strong> All rights reserved.
    </footer>
</div><!-- ./wrapper -->


</body>
</html>