<%@ page import="br.unirn.projeto20151.security.User" %>

<header class="main-header">
    <!-- Logo -->
    <g:link uri="/" class="logo"><b>Vida em Família</b></g:link>
    %{--<a href="index2.html" class="logo"><b>Admin</b>LTE</a>--}%
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <sec:ifLoggedIn>
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </sec:ifLoggedIn>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <sec:ifLoggedIn>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell-o"></i>
                            <span class="label label-warning">10</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have 10 notifications</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-red"></i> 5 new members joined
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#">
                                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user text-red"></i> You changed your username
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"><a href="#">View all</a></li>
                        </ul>
                    </li>
                </sec:ifLoggedIn>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        %{--<asset:image src="user2-160x160.jpg" class="user-image" alt="User Image"/>--}%
                        <span class="hidden-xs">Minha Conta</span>
                    </a>
                    <ul class="dropdown-menu">

                        <sec:ifNotLoggedIn>
                            <!-- User image -->
                            <li class="user-header">
                                %{--<asset:image src="user2-160x160.jpg" class="img-circle" alt="User Image"/>--}%
                                <i class="fa fa-user fa-4x" ></i>
                                <p>
                                    Área de Usuários
                                    <small>Perfil, Autenticação e Cadastro</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-6 text-center">
                                    <g:link controller="login" action="auth" class="btn btn-flat btn-primary"> Entrar </g:link>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <g:link controller="user" action="create" class="btn btn-flat btn-warning"> Cadastrar</g:link>
                                </div>
                            </li>
                        </sec:ifNotLoggedIn>

                        <sec:ifLoggedIn>
                            <!-- User image -->
                            <li class="user-header">
                                <i class="fa fa-user fa-4x" ></i>
                                <p>
                                    <sec:loggedInUserInfo field="username"/>
                                    %{--Área de Usuários--}%
                                    <small>Perfil, Autenticação e Cadastro</small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="col-xs-12 text-center">
                                    <g:link controller="familia" action="myFamily" class="btn btn-block btn-default btn-flat"> Minha Família </g:link>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <g:link controller="user" action="myProfile" class="btn btn-flat btn-primary"> Meu Perfil </g:link>
                                </div>
                                <div class="pull-right">
                                    <g:link controller="secure" action="logout" class="btn btn-danger btn-flat"
                                    > Sair </g:link>
                                </div>
                            </li>
                        </sec:ifLoggedIn>

                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
