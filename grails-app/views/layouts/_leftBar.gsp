
<!-- Left side column. contains the logo and sidebar -->

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        %{--<div class="user-panel">
            <div class="pull-left image">
                <asset:image src="user2-160x160.jpg" class="img-circle" alt="User Image"/>
                --}%%{--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />--}%%{--
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>--}%
        <!-- search form -->
        %{--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>--}%
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MENU</li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-rocket"></i> <span>Postagens</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><g:link controller="postagem" action="meusPosts"> <i class="fa fa-circle-o"></i> Meus Posts </g:link></li>
                <li><g:link controller="postagem" action="create"> <i class="fa fa-circle-o"></i> Cadastrar Post </g:link></li>
                <li><g:link controller="postagem" action="buscar"> <i class="fa fa-circle-o"></i> Buscar Post </g:link></li>
                %{--<li>
                    <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>--}%
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-smile-o"></i> <span>Família</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><g:link controller="familia" action="myFamily"><i class="fa fa-circle-o"></i> Minha Família </g:link></li>
                <li><g:link controller="user" action="myProfile"><i class="fa fa-circle-o"></i> Meu Perfil </g:link></li>

                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_PARENT">
                    <li><g:link controller="familia" action="gerenciar"><i class="fa fa-circle-o"></i> Gerenciar </g:link></li>
                    <li><g:link controller="familia" action="itensBloqueados"><i class="fa fa-circle-o"></i> Itens Bloqueados </g:link></li>
                </sec:ifAnyGranted>

            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-list"></i> <span>Categorias</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li><g:link controller="categoria" action="index"><i class="fa fa-circle-o"></i> Gerenciar Categorias </g:link></li>
                </sec:ifAnyGranted>
                <li><g:link controller="categoria" action="listar"><i class="fa fa-circle-o"></i> Listar Categorias </g:link></li>

            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-tags"></i> <span>Tags</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <sec:ifAnyGranted roles="ROLE_ADMIN">
                    <li><g:link controller="tag" action="index"><i class="fa fa-circle-o"></i> Gerrenciar Tags </g:link></li>
                </sec:ifAnyGranted>
                <li><g:link controller="tag" action="listar"><i class="fa fa-circle-o"></i> Listar Tags</g:link></li>
                <li><g:link controller="tag" action="ultimasTags"><i class="fa fa-circle-o"></i> Últimas Tags</g:link></li>
                <li><g:link controller="tag" action="tagsMaisUsadas"><i class="fa fa-circle-o"></i> Tags Mais Usadas</g:link></li>
                <li><g:link controller="tag" action="buscarTag"><i class="fa fa-circle-o"></i> Buscar por Tag </g:link></li>

            </ul>
        </li>



        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-danger"></i> Important</a></li>
        <li><a href="#"><i class="fa fa-circle-o text-warning"></i> Warning</a></li>
        <li><a href="#"><i class="fa fa-circle-o text-info"></i> Information</a></li>
    </ul>
    </section>
    <!-- /.sidebar -->
</aside>

