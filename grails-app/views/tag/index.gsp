
<%@ page import="br.unirn.projeto20151.conteudo.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>

<ol class="breadcrumb">
    %{-- <li><a href="/projeto/"><i class="fa fa-home"></i> Principal 1</a></li> --}%
    %{-- <li><a href="${g.createLink(uri: '/')}"><i class="fa fa-home"></i> Principal 2</a></li>  --}%
    <li> <g:link uri="/"><i class="fa fa-home"></i> Principal </g:link></li>
    <li class="active">Listagem de Tags</li>
</ol>

<div class="row">
    <div class="col-lg-2 col-lg-offset-10">
        %{-- <g:link controller="tag" action="create" class="btn btn-danger"> Nova Tag </g:link> --}%
        %{-- <g:link controller="tag" action="create" class="btn btn-warning"> Nova Tag </g:link> --}%
        %{-- <g:link controller="tag" action="create" class="btn btn-info"> Nova Tag </g:link> --}%
        <g:link controller="tag" action="create" class="btn btn-primary "> Nova Tag </g:link>
    </div>
</div>

<div id="list-tag" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table class="table table-hover">
        <thead>
        <tr>

            <g:sortableColumn property="nome" title="${message(code: 'tag.nome.label', default: 'Nome')}" />

            <g:sortableColumn property="dateCreated" title="${message(code: 'tag.dateCreated.label', default: 'Date Created')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${tagInstanceList}" status="i" var="tagInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${tagInstance.id}">${fieldValue(bean: tagInstance, field: "nome")}</g:link></td>

                <td><g:formatDate date="${tagInstance.dateCreated}" format="dd/MM/yyyy hh:mm" /></td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${tagInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
