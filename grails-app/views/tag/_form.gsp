<%@ page import="br.unirn.projeto20151.conteudo.Tag" %>



<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="tag.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="nome" cols="40" rows="5" maxlength="255" required="" value="${tagInstance?.nome}"/>

</div>

