<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Vida em Família | Home</title>

</head>
<body>

<!-- Content Header (Page header) -->
%{--<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>--}%

<!-- Main content -->
<section class="content">

<!-- Main row -->
    <sec:ifLoggedIn>
        <div id="mainLoggedId">
            <g:render template="mainLogged" />
        </div>
    </sec:ifLoggedIn>

    <sec:ifNotLoggedIn>
        <div id="mainNotLoggedId">
            <g:render template="mainNotLogged"/>
        </div>
    </sec:ifNotLoggedIn>
    <!-- /.row (main row) -->

</section><!-- /.content -->

</body>
</html>
