

<!-- chat item -->
<g:each in="${ultimasPostagens}" var="post">
    <div class="well well-sm">
        <div class="item">
            %{--<asset:image src="user4-128x128.jpg" alt="user image" class="online"/>--}%
            %{--<img src="dist/img/user4-128x128.jpg" alt="user image" class="online"/>--}%
            <i class="fa fa-user fa-2x"></i>
            <p class="message">
                <a href="javascript:;" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> ${post?.dateCreated?.format("dd/MM/yyyy HH:mm")}</small>
                    <strong>Usuário:</strong> ${post?.dono}
                </a>
                <strong>Título:</strong> ${post?.titulo}
            </p>
            <div class="attachment">
                <h4>Descrição:</h4>
                <p class="text text-green ">
                    ${post?.descricao}
                </p>
                <br/>
                <p class="pull-right">
                     ${post?.dono?.familia}
                </p>
                <p class="pull-left">
                     <strong>Tags:</strong> ${post?.tags}
                </p>
                %{--<div class="pull-right">
                    <button class="btn btn-primary btn-sm btn-flat">Open</button>
                </div>--}%
            </div><!-- /.attachment -->
        </div>
    </div>
</g:each>


%{--<div class="item">
    --}%%{--<asset:image src="user4-128x128.jpg" alt="user image" class="online"/>--}%%{--
    <i class="fa fa-user fa-3x"></i>
    <p class="message">
        <a href="#" class="name">
            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
            Mike Doe
        </a>
        I would like to meet you to discuss the latest news about
        the arrival of the new theme. They say it is going to be one the
        best themes on the market
    </p>
    <div class="attachment">
        <h4>Attachments:</h4>
        <p class="filename">
            Theme-thumbnail-image.jpg
        </p>
        <div class="pull-right">
            <button class="btn btn-primary btn-sm btn-flat">Open</button>
        </div>
    </div><!-- /.attachment -->
</div>--}%<!-- /.item -->