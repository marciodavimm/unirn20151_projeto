package br.unirn.projeto20151.security

import br.unirn.projeto20151.base.Familia

class User {

	transient springSecurityService

    String fullName
    Date dateCreated
    Date lastUpdated

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

    static belongsTo = [familia:Familia]

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false
        fullName nullable: true
	}

	static mapping = {
		password column: '`password`'
        table 'users'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

    @Override
    String toString() {
//        return super.toString()
        "${fullName ? "${fullName} (${username})" :username}"
    }
}
