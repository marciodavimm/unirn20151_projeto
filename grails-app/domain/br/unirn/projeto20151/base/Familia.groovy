package br.unirn.projeto20151.base

import br.unirn.projeto20151.conteudo.Tag
import br.unirn.projeto20151.security.User

class Familia {
    String nome
    User pai
    User mae
    Date dateCreated
    Date lastUpdated

    static hasMany = [integrantes : User]
    static mapping = {
        integrantes cascade: 'all'
    }

    static constraints = {
        nome unique: true
        pai(nullable: true)
        mae(nullable: true)
    }

    @Override
    String toString() {
        "${nome}"
    }
}
