package br.unirn.projeto20151.conteudo

class Categoria {

    String nome

    Date dateCreated
    Date lastUpdated

    static mapping = {
    }

    static constraints = {
        nome(unique: true)
    }
}
