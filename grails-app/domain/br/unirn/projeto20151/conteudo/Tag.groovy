package br.unirn.projeto20151.conteudo

import br.unirn.projeto20151.base.Familia

class Tag {

    String nome

    Date dateCreated

    static mapping = {
    }

    static constraints = {
        nome nullable:false,blank:false,size:0..255,unique:true
    }

    @Override
    String toString() {
        nome
    }
}
