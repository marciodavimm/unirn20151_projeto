package br.unirn.projeto20151.conteudo

import br.unirn.projeto20151.security.User

class Postagem {

    String titulo
    String descricao
    User dono

    Categoria categoria

    Date dateCreated
    Date lastUpdated

    static hasMany = [tags: Tag]

    static mapping = {
    }

    static constraints = {
        titulo(unique: true)
        descricao(size: 0..255)
        categoria(nullable: true)
    }

    @Override
    String toString() {
//        return super.toString()
        "${titulo} (${dono})"
    }
}
