import br.unirn.projeto20151.base.Familia
import br.unirn.projeto20151.security.Role
import br.unirn.projeto20151.security.User
import br.unirn.projeto20151.security.UserRole

class BootStrap {

    def init = { servletContext ->

        def adminRole = Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def parentRole = Role.findByAuthority('ROLE_PARENT') ?: new Role(authority: 'ROLE_PARENT').save(flush: true)
        def childRole = Role.findByAuthority('ROLE_CHILD') ?: new Role(authority: 'ROLE_CHILD').save(flush: true)

        def familiaAdminMarcio = Familia.findByNome("AdministradoresMarcio") ?: new Familia(nome: "AdministradoresMarcio").save(flush: true, failOnError: true)
        def familiaAdminFabio = Familia.findByNome("AdministradoresFabio") ?: new Familia(nome: "AdministradoresFabio").save(flush: true, failOnError: true)
        def familiaTeste = Familia.findByNome("FamiliaTeste") ?: new Familia(nome: "FamiliaTeste").save(flush: true, failOnError: true)

        def marcio = User.findByUsername('marcio') ?: new User(username: 'marcio', password: 'marcio', familia: familiaAdminMarcio).save(flush: true, failOnError: true)
        def fabio = User.findByUsername('fabio') ?: new User(username: 'fabio', password: 'fabio', familia: familiaAdminFabio).save(flush: true, failOnError: true)
        def filhoTeste = User.findByUsername('filhoteste') ?: new User(username: 'filhoteste', password: 'filhoteste', familia: familiaTeste).save(flush: true, failOnError: true)
        def paiTeste = User.findByUsername('paiteste') ?: new User(username: 'paiteste', password: 'paiteste', familia: familiaTeste).save(flush: true, failOnError: true)

        familiaTeste.pai = paiTeste
//        familiaTeste.addToIntegrantes(filhoTeste)
        familiaTeste.save flush: true

        UserRole.create marcio, adminRole, true
        UserRole.create fabio, adminRole, true
        UserRole.create filhoTeste, childRole, true
        UserRole.create paiTeste, parentRole, true

//        assert User.count() == 2
//        assert Role.count() == 3
//        assert UserRole.count() == 2
    }

    def destroy = {
    }
}
